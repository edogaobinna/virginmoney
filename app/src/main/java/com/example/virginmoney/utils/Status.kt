package com.example.virginmoney.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}