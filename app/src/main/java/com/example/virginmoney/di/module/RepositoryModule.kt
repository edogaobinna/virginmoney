package com.example.virginmoney.di.module

import com.example.virginmoney.data.repository.MainRepository
import org.koin.dsl.module

val repoModule = module {
    single {
        MainRepository(get())
    }
}