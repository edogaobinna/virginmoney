package com.example.virginmoney.di.module

import com.example.virginmoney.ui.dashboard.DashboardViewModel
import com.example.virginmoney.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.scope.get
import org.koin.dsl.module


val viewModelModule = module {
    viewModel {
        HomeViewModel(get(),get())
    }
    viewModel {
        DashboardViewModel(get(),get())
    }
}
