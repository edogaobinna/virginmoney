package com.example.virginmoney.data.api

import com.example.virginmoney.data.model.People
import com.example.virginmoney.data.model.Rooms
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("people")
    suspend fun getPeople(): Response<List<People>>

    @GET("rooms")
    suspend fun getRooms(): Response<List<Rooms>>
}