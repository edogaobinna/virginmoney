package com.example.virginmoney.data.model

import com.squareup.moshi.Json

data class Rooms(
    @Json(name = "id")
    val id: String = "",
    @Json(name = "name")
    val name: String = "",
    @Json(name = "max_occupancy")
    val max_occupancy: Int = 0,
    @Json(name = "is_occupied")
    val is_occupied: Boolean = false,
    @Json(name = "created_at")
    val created_at: String = ""
)