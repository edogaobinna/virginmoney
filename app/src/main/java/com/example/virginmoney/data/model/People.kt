package com.example.virginmoney.data.model

import com.squareup.moshi.Json

data class People(
    @Json(name = "avatar")
    val avatar: String = "",
    @Json(name = "phone")
    val phone: String = "",
    @Json(name = "firstName")
    val firstName: String = "",
    @Json(name = "longitude")
    val longitude: Double = 0.0,
    @Json(name = "favouriteColor")
    val favouriteColor: String = "",
    @Json(name = "email")
    val email: String = "",
    @Json(name = "jobTitle")
    val jobTitle: String = "",
    @Json(name = "createdAt")
    val createdAt: String = "",
    @Json(name = "latitude")
    val latitude: Double = 0.0,
    @Json(name = "lastName")
    val lastName: String = ""
)