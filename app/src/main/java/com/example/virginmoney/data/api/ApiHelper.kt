package com.example.virginmoney.data.api

import com.example.virginmoney.data.model.People
import com.example.virginmoney.data.model.Rooms
import retrofit2.Response

interface ApiHelper {
    suspend fun getPeople(): Response<List<People>>
    suspend fun getRooms(): Response<List<Rooms>>
}