package com.example.virginmoney.data.repository

import com.example.virginmoney.data.api.ApiHelper

class MainRepository (private val apiHelper: ApiHelper) {

    suspend fun getPeople() =  apiHelper.getPeople()

    suspend fun getRooms() =  apiHelper.getRooms()

}