package com.example.virginmoney.data.api

import com.example.virginmoney.data.model.People
import com.example.virginmoney.data.model.Rooms
import retrofit2.Response

class ApiHelperImpl(private val apiService: ApiService) : ApiHelper  {
    override suspend fun getPeople(): Response<List<People>> = apiService.getPeople()
    override suspend fun getRooms(): Response<List<Rooms>> = apiService.getRooms()
}