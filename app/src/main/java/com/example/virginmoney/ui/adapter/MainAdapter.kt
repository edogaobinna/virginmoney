package com.example.virginmoney.ui.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.virginmoney.R
import com.example.virginmoney.data.model.People
import com.example.virginmoney.utils.NavigationAction
import kotlin.reflect.KFunction2

class MainAdapter(
    private val people: ArrayList<People>, private val itemClickCallback: (People,NavigationAction) -> Unit
) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: People, menuItemClicked: (people: People, navType: NavigationAction) -> Unit) {
            itemView.findViewById<AppCompatTextView>(R.id.textViewUserName).text = "${user.firstName} ${user.lastName}"
            itemView.findViewById<AppCompatTextView>(R.id.textViewUserEmail).text = user.email
            Glide.with(itemView.findViewById<ImageView>(R.id.imageViewAvatar).context)
                .load(user.avatar)
                .into(itemView.findViewById(R.id.imageViewAvatar))

            itemView.findViewById<ConstraintLayout>(R.id.container).setOnClickListener {
                menuItemClicked(user,NavigationAction.ITEM_CLICK)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout, parent,
                false
            )
        )

    override fun getItemCount(): Int = people.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(people[position], itemClickCallback)

    fun addData(list: List<People>) {
        people.addAll(list)
    }
}