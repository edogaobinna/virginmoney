package com.example.virginmoney.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.virginmoney.data.model.People
import com.example.virginmoney.data.network.NetworkHelper
import com.example.virginmoney.data.repository.MainRepository
import com.example.virginmoney.utils.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class HomeViewModel(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper,
    private val coroutineScopeProvider: CoroutineScope? = null
) : ViewModel() {

    private val _users = MutableLiveData<Resource<List<People>>>()
    private val coroutineScope = getViewModelScope(coroutineScopeProvider)
    val people: LiveData<Resource<List<People>>>
        get() = _users

    init {
        fetchUsers()
    }

    private fun fetchUsers() {
        coroutineScope.launch {
            _users.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getPeople().let {
                    if (it.isSuccessful) {
                        _users.postValue(Resource.success(it.body()))
                    } else _users.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else _users.postValue(Resource.error("No internet connection", null))
        }
    }

    fun ViewModel.getViewModelScope(coroutineScope: CoroutineScope?) =
        coroutineScope ?: this.viewModelScope
}