package com.example.virginmoney.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.virginmoney.data.model.People
import com.example.virginmoney.data.model.Rooms
import com.example.virginmoney.data.network.NetworkHelper
import com.example.virginmoney.data.repository.MainRepository
import com.example.virginmoney.utils.Resource
import kotlinx.coroutines.launch

class DashboardViewModel(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper
) : ViewModel() {

    private val _rooms = MutableLiveData<Resource<List<Rooms>>>()
    val rooms: LiveData<Resource<List<Rooms>>>
        get() = _rooms

    init {
        fetchUsers()
    }

    private fun fetchUsers() {
        viewModelScope.launch {
            _rooms.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getRooms().let {
                    if (it.isSuccessful) {
                        _rooms.postValue(Resource.success(it.body()))
                    } else _rooms.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else _rooms.postValue(Resource.error("No internet connection", null))
        }
    }
}