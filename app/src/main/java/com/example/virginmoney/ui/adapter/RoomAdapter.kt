package com.example.virginmoney.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.virginmoney.R
import com.example.virginmoney.data.model.Rooms

class RoomAdapter(
    private val rooms: ArrayList<Rooms>
) : RecyclerView.Adapter<RoomAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(rooms: Rooms) {
            itemView.findViewById<AppCompatTextView>(R.id.roomName).text = "Room Name: ${rooms.name}"
            itemView.findViewById<AppCompatTextView>(R.id.roomOccupancy).text = "Room Max Occupancy: ${rooms.max_occupancy.toString()}"
            itemView.findViewById<AppCompatTextView>(R.id.roomOccupied).text = "is room occupied: ${if(rooms.is_occupied) "Yes" else "No"}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.room_layout, parent,
                false
            )
        )

    override fun getItemCount(): Int = rooms.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(rooms[position])

    fun addData(list: List<Rooms>) {
        rooms.addAll(list)
    }
}