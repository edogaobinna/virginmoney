package com.example.virginmoney.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.virginmoney.R
import com.example.virginmoney.data.model.People
import com.example.virginmoney.databinding.FragmentHomeBinding
import com.example.virginmoney.ui.adapter.MainAdapter
import com.example.virginmoney.utils.NavigationAction
import com.example.virginmoney.utils.Status
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val homeViewModel : HomeViewModel by viewModel()
    private lateinit var adapter: MainAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        setupUI()
        setupObserver()
        return binding.root
    }


    private fun setupUI() {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = MainAdapter(arrayListOf(), ::onMenuItemClicked)
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerView.context,
                (binding.recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        binding.recyclerView.adapter = adapter

    }

    private fun setupObserver() {
        homeViewModel.people.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    it.data?.let { users -> renderList(users) }
                    binding.recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun onMenuItemClicked(people: People, navType:NavigationAction) {
       when(navType){
           NavigationAction.ITEM_CLICK -> {
               val bundle = Bundle()
               bundle.putString("name", "${people.firstName} ${people.lastName}")
               bundle.putString("avatar", people.avatar)
               bundle.putString("phone", people.phone)
               bundle.putString("email", people.email)
               bundle.putString("job", people.jobTitle)
               Navigation.findNavController(binding.root).navigate(R.id.homefragment_to_detailfragment, bundle)
           }
       }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun renderList(users: List<People>) {
        adapter.addData(users)
        adapter.notifyDataSetChanged()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}