package com.example.virginmoney

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.testing.TestLifecycleOwner
import com.example.virginmoney.data.network.NetworkHelper
import com.example.virginmoney.data.repository.MainRepository
import com.example.virginmoney.ui.home.HomeViewModel
import com.example.virginmoney.utils.Status
import com.google.common.truth.Truth
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class HomeViewModelTest {
    private val mainRepo: MainRepository = mockk() // I used Mockk for mocking, but you can use any other mocking framework
    private val networkHelper: NetworkHelper = mockk()

     // your class under test
    private val testDispatcher = StandardTestDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)


    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun viewModelDataTest() {
       val homeViewModel = HomeViewModel(mainRepo,networkHelper,testScope)
        homeViewModel.people.observe(TestLifecycleOwner()) { it ->
            it.data?.let { users ->
                Truth.assertThat(users[0].firstName).isEqualTo("maybell")
            }

        }

        // do assertions
    }
}